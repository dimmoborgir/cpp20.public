## ШАД C++ 2020

В этом репозитории находятся:

* `build_script/main.py` - скрипт автоматический проверки ваших решений
* `build_script/tests/` - публичные тесты для задач (файлы с приватными тестами есть, но они пусты)
* `example_solution` - тестовая задача, которую можно взять за основу. Отсюда стоит брать `Makefile`. 

### Тесты
По доступности тесты делятся на:

* `public*.cpp` - публичные (доступны вам, можно изучить),
* `private*.cpp` - приватные (недоступны вам, запускаются только в Anytask).

По проверяемой функциональности тесты делятся на:

* базовые (`public.cpp, private.cpp`) - позволяют получить базовый балл за задачу,
* продвинутые (`public_advanced.cpp, private_advanced.cpp`) - позволяют получить максимальный балл за задачу.

### Запуск проверяющей системы

Есть возможность запустить локально тот же скрипт, который будет проверять ваши решения в Anytask.

При локальном запуске вместо private-тестов используются пустые "заглушки".

Убедимся, что скрипт работает - склонируем репозиторий и проверим тестовую задачу:

```
git clone git@bitbucket.org:dimmoborgir/cpp20.public.git
cd cpp20.public
build_script/main.py "0. Example" ./example_solution
```

Ожидаемый вывод:
```
INFO     2020-10-26 01:00:46,168: Coverage results:
Filename                         Regions    Missed Regions     Cover   Functions  Missed Functions  Executed       Lines      Missed Lines     Cover
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/tmp/tmpq1aqu179/example.cpp           4                 1    75.00%           1                 0   100.00%           6                 2    66.67%
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TOTAL                                  4                 1    75.00%           1                 0   100.00%           6                 2    66.67%

INFO     2020-10-26 01:00:46,168: Run with sanitizer 'address':
INFO     2020-10-26 01:00:53,369:   coverage         : Tests: 1; Failed: 0 | SUCCESS
INFO     2020-10-26 01:00:53,383:   public           : Tests: 1; Failed: 0 | SUCCESS
INFO     2020-10-26 01:00:53,397:   private          : Tests: 0; Failed: 0 | SUCCESS
WARNING  2020-10-26 01:00:53,419:   public_advanced  : Tests: 1; Failed: 1 (PublicAdvanced.BigNumbers)
INFO     2020-10-26 01:00:53,441:   private_advanced : Tests: 0; Failed: 0 | SUCCESS
INFO     2020-10-26 01:00:53,442: Run with sanitizer 'undefined':
INFO     2020-10-26 01:01:01,530:   coverage         : Tests: 1; Failed: 0 | SUCCESS
INFO     2020-10-26 01:01:01,534:   public           : Tests: 1; Failed: 0 | SUCCESS
INFO     2020-10-26 01:01:01,538:   private          : Tests: 0; Failed: 0 | SUCCESS
WARNING  2020-10-26 01:01:01,543:   public_advanced  : Tests: 1; Failed: 1 (PublicAdvanced.BigNumbers)
INFO     2020-10-26 01:01:01,547:   private_advanced : Tests: 0; Failed: 0 | SUCCESS
```

### Как писать, отлаживать решение 
1. Создаем отдельный каталог, например `foo`
2. Копируем туда основные файлы из примера: `cp example_solution/{Makefile,entry_point.h,coverage.cpp} foo`
3. Копируем туда же файлы тестов: `cp "build_script/tests/X. Task Name/*" foo` 
4. Создаем нужные файлы (`*.cpp, *.h`) с кодом решения
5. Инклудим все `*.h` файлы в `entry_point.h`
6. Правим `Makefile`, для каждого `cpp` файла должно быть:
   - по одной цели вида: `{NAME}.o: $(SRCD)/{NAME}.cpp`
   - в цели `coverage`, `public`, `public_advanced`, `private`, `private_advanced` добавить все `{NAME}.o`
7. Смотрим авторские тесты в `public.cpp`, `public_advanced.cpp`
8. Пишем свои (новые) тесты в `coverage.cpp`
9. Запускаем тесты: `build_script/main.py ./foo`


**Если вы хотите запускать тесты командой `make` (быстрее)**

1. Склонируйте (находясь в директории решения `foo`): `git clone https://github.com/google/googletest.git`
2. Соберите тесты командой `make`
3. Запустите нужные тесты: `./public`
```
Running main() from googletest/googletest/src/gtest_main.cc
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from Public
[ RUN      ] Public.SimpleTest
[       OK ] Public.SimpleTest (0 ms)
[----------] 1 test from Public (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (0 ms total)
[  PASSED  ] 1 test.
```

### Как отправить решение
Запаковываем каталог `foo` в архив. Прицепляем в anytask.

### Google Test Library
* https://github.com/google/googletest
* https://github.com/google/googletest/blob/master/googletest/docs/primer.md
