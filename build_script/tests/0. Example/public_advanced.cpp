#include "entry_point.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

namespace Example {
    TEST(PublicAdvanced, BigNumbers) {
        ASSERT_EQ(2000000, sum(1000000, 1000000));
    }
}