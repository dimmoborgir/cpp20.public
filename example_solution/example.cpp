#include "example.h"

int sum(int a, int b) {
    if (a < 1000) {
        return a + b;
    }
    return a - b;
}
